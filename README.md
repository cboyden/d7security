# D7Security

The D7Security group is providing unofficial extended support for selected Drupal 7 modules and themes.

Please check the documentation in the [Wiki](https://gitlab.com/d7security/d7security/-/wikis/Home).

🚧 The goals and processes of this group are currently a work in progress and being discussed at #1 🚧

🚨 All security reports against Drupal 7 should still be reported via [security.drupal.org](https://security.drupal.org/node/add/project-issue/drupal) first! 🚨
